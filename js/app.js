(function() {
  'use strict';

  var App = {
    showLegendaImgs: function () {
      var els = document.querySelectorAll('img');

      els.forEach(function(el) {
        var legendaEl = document.createElement('div');
        legendaEl.className = 'teste';
        legendaEl.textContent = el.alt;

        el.parentNode.insertBefore(legendaEl, el.nextSibling);
      });
    }
  };

  window.App = App;

}());